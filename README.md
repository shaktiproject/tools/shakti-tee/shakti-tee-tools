shakti-tee-tools (forked from github.com/riscv/riscv-tools) 
===========================================================================

This repository houses a set of RISC-V simulators, compilers, and other
tools, which are necessary for the SHAKTI-TEE (Trusted Execution Environment) project.

Included Projects:

* [shakti-tee-isa-sim](https://gitlab.com/sl33k/shakti-tee-isa-sim), the SHAKTI-TEE ISA simulator
* [riscv-fesvr](https://github.com/riscv/riscv-fesvr/), the host side of
a simulation tether that services system calls on behalf of a target machine
* [riscv-gnu-toolchain](https://github.com/riscv/riscv-gnu-toolchain), a complete toolchain including gcc and gdb and various other GNU tools.
* [OpenOCD](http://openocd.org/)

# <a name="quickstart"></a>Quickstart

	$ git submodule update --init --recursive
	$ export RISCV=/path/to/install/riscv/toolchain
	$ ./build.sh


Ubuntu packages needed:

	$ sudo apt-get install autoconf automake autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev libusb-1.0-0-dev gawk build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev device-tree-compiler pkg-config libexpat-dev

Fedora packages needed:

	$ sudo dnf install autoconf automake @development-tools curl dtc libmpc-devel mpfr-devel gmp-devel libusb-devel gawk gcc-c++ bison flex texinfo gperf libtool patchutils bc zlib-devel expat-devel

_Note:_ This requires a compiler with C++11 support (e.g. GCC >= 4.8).
To use a compiler different than the default, use:

	$ CC=gcc-5 CXX=g++-5 ./build.sh
